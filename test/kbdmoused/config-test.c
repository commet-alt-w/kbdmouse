#include "../../src/kbdmoused/config.c"
#include <criterion/criterion.h>

#define TEST_CONFIG_FILE "data/kbdm.conf"

static const kbdm_config_t *conf;

static void
setup(void) {
  conf = get_config();
}

static void
teardown(void) {
  cleanup_config();
}

TestSuite(config_ts, .init = setup, .fini = teardown);
TestSuite(config_file_ts);

Test(config_ts, init_cleanup_t) {
  // assert config is not null
  cr_assert(conf != NULL);
  // cleanup config
  cleanup_config();
  // get config again
  conf = get_config();
  // assert config is not null
  cr_assert(conf != NULL);
}

Test(config_file_ts, init_config_file_t) {
  // init config file
  cr_assert(init_config_file(TEST_CONFIG_FILE) == 0);
  // assert file found
  cr_assert(config_present() == true);
  // cleanup config
  cleanup_config();
}
