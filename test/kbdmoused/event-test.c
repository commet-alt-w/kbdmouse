#include "../../src/kbdmoused/event.c"
#include <criterion/criterion.h>

// test macro constants
#define NUMEVENTS 3

// test vars
static kbdm_event_t **e;
static int values[NUMEVENTS] = {1, 2, 3};

/*
 * init_event_queue_state
 *
 * set initial event queue state
 */
static void
init_event_queue_state(void) {
  // init event queue
  kbdm_event_init();
  // specify atomic value
  is_empty = true;
}

/*
 * setup
 *
 * initialize data needed for tests
 */
static void setup(void) {
  // set initial event queue state
  init_event_queue_state();
  // init events array
  e = calloc(NUMEVENTS, sizeof(kbdm_event_t *));
  // init input events
  for (int i = 0; i < NUMEVENTS; i++) {
    e[i] = calloc(1, sizeof(kbdm_event_t));
    e[i]->ev = calloc(1, sizeof(struct input_event));
    e[i]->ev->value = values[i];
  }
}

/*
 * teardown
 *
 * free tests memory
 */
static void teardown(void) {
  // free memory
  for (int i = 0; i < NUMEVENTS; i++) {
    free(e[i]->ev);
    free(e[i]);
  }
  free(e);
}

/*
 * teardown_minimal
 *
 * free pointer to array items
 */
static void teardown_minimal(void) {
  free(e);
}

// init test suites
TestSuite(event, .init = setup, .fini = teardown);
TestSuite(event_cleanup, .init = setup, .fini = teardown_minimal);

// test pushing/pulling events
Test(event, push_pull_event) {
  // check if queue is empty
  cr_assert(queue_is_empty() == true);
  
  // push events
  for (int i = 0; i < NUMEVENTS; i++) {
    push_event(e[i]);
  }

  // pull events
  for (int i = 0; i < NUMEVENTS; i++) {
    const kbdm_event_t *tmp = NULL;
    tmp = pull_event();
    cr_assert(tmp->ev->value == values[i]);
  }

  // check if queue is empty
  cr_assert(queue_is_empty() == true);
}

// test cleaning up events
Test(event_cleanup, push_cleanup_events) {
  // check queue is empty
  cr_assert(queue_is_empty() == true);

  // push events
  for (int i = 0; i < NUMEVENTS; i++) {
    push_event(e[i]);
  }

  // call cleanup
  kbdm_event_cleanup();

  // check if queue is empty
  cr_assert(queue_is_empty() == true);
}
