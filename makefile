.PHONY: release relwithdebinfo debug test defualt clean-release clean-relwithdebinfo clean-debug clean-test clean

build-dir = build
release-dir = release
rwdi-dir = relwithdebinfo
debug-dir = debug
test-dir = test

release:
	@cmake -S . -B $(build-dir)/$(release-dir) -DCMAKE_BUILD_TYPE=Release
	@cmake --build $(build-dir)/$(release-dir)

relwithdebinfo:
	@cmake -S . -B $(build-dir)/$(rwdi-dir) -DCMAKE_BUILD_TYPE=RelWithDebInfo
	@cmake --build $(build-dir)/$(rwdi-dir)

debug:
	@cmake -S . -B $(build-dir)/$(debug-dir) -DCMAKE_BUILD_TYPE=Debug
	@cmake --build $(build-dir)/$(debug-dir)

test:
	@cmake -S . -B $(build-dir)/$(test-dir) -DCMAKE_BUILD_TYPE=Test
	@cmake --build $(build-dir)/$(test-dir)

clean-release:
	@rm -rf $(build-dir)/$(release-dir)

clean-relwithdebinfo:
	@rm -rf $(build-dir)/$(rwdi-dir)

clean-debug:
	@rm -rf $(build-dir)/$(debug-dir)

clean-test:
	@rm -rf $(build-dir)/$(test-dir)

clean:
	@rm -rf $(build-dir)

default:
	release
