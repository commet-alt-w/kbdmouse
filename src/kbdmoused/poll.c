#include "poll.h"
#include "scheduler.h"
#include "event.h"
#include "../kbdm.h"
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <syslog.h>
#include <fcntl.h>
#include <pthread.h>
#include <errno.h>
#include <libevdev/libevdev.h>

#ifdef DEBUG
/*
 * log_event
 *
 * log arrow keypress/release
 */
static void
log_event(struct input_event *ev) {
  switch(ev->code) {
  case KEY_UP:
    if (ev->value == 1) {
      kbdmlog(LOG_INFO, "<UP> key pressed");
    } else if (ev->value == 0) {
      kbdmlog(LOG_INFO, "<UP> key released");
    }
    break;
  case KEY_RIGHT:
    if (ev->value == 1) {
      kbdmlog(LOG_INFO, "<RIGHT> key pressed");
    } else if (ev->value == 0) {
      kbdmlog(LOG_INFO, "<RIGHT> key released");
    }
    break;
  case KEY_DOWN:
    if (ev->value == 1) {
      kbdmlog(LOG_INFO, "<DOWN> key pressed");
    } else if (ev->value == 0) {
      kbdmlog(LOG_INFO, "<DOWN> key released");
    }
    break;
  case KEY_LEFT:
    if (ev->value == 1) {
      kbdmlog(LOG_INFO, "<LEFT> key pressed");
    } else if (ev->value == 0) {
      kbdmlog(LOG_INFO, "<LEFT> key released");
    }
    break;
  }
}
#endif

/*
 * is_arrow_key
 *
 * check if the input_event code was
 * from an arrow key being pressed or released
 *
 * return true if code is related to an arrow key
 * return false otherwise
 */
static bool
is_arrow_key(int code) {
  if (code == KEY_UP ||
      code == KEY_RIGHT ||
      code == KEY_DOWN ||
      code == KEY_LEFT) {
    return true;
  }
  return false;
}

static kbdm_event_t *
new_kbdm_event(struct input_event *ev) {
  // create new kbdm event item
  kbdm_event_t *e = calloc(1, sizeof(kbdm_event_t));
  // init input event
  e->ev = calloc(1, sizeof(struct input_event));
  // copy ev values we need
  e->ev->code = ev->code;
  e->ev->value = ev->value;
  // return
  return e;
}

/*
 * process_event
 *
 * process libevdev input event
 */
static void
process_event(int rc, struct input_event *ev) {
  switch(rc) {
  case LIBEVDEV_READ_STATUS_SYNC:
#ifdef DEBUG
    kbdmlog(LOG_INFO, "dropped libevdev event");
#endif
    break;
  case LIBEVDEV_READ_STATUS_SUCCESS:
    // check if there was a keypress/release event
    if (ev->type == EV_KEY) {
      // check if the event was related to an arrow key
      if (is_arrow_key(ev->code)) {
        // log event
#ifdef DEBUG
        log_event(ev);
#endif
        // create new kbdm event
        kbdm_event_t *e = new_kbdm_event(ev);
        // push to event queue
        push_event(e);
      }
    }
    break;
  }
}

/*
 * init_thread_vars
 *
 * initialize all thread variables
 */
static void
init_thread_vars(struct kbd_info **kbdip, const char *devname) {
  // get kbdi
  struct kbd_info *kbdi = NULL;
  // init keyboard info
  kbdi = calloc(1, sizeof(struct kbd_info));
  // init evs
  kbdi->ev = calloc(1, sizeof(struct input_event));
  // init fd
  kbdi->fd = -1;
  // init devname
  kbdi->devname = devname;
  // set kbdi
  *kbdip = kbdi;
}

/*
 * prepare_polling
 *
 * open necessary file descriptors
 * and prepare libevdev structs
 *
 * return 0 on success
 * return -1 on failure
 */
static int
prepare_polling(struct kbd_info **kbdip) {
  struct kbd_info *kbdi = *kbdip;
  // open input device for reading
  kbdi->fd = open(kbdi->devname, FFLAGS);

  // check for errors
  if (kbdi->fd < 0) {
    return -1;
  }

  // initialize ldev
  int rc = libevdev_new_from_fd(kbdi->fd, &kbdi->ldev);

  // check for errors
  if (rc < 0) {
    return -1;
  }

  return 0;
}

/*
 * start_polling
 *
 */
static int
start_polling(struct kbd_info *kbdi) {
  // init return val
  int rc = -1;

  // main loop, poll for input
  do {
    // get event
    rc = libevdev_next_event(kbdi->ldev, LDEVFLAGS, kbdi->ev);
    // process event
    process_event(rc, kbdi->ev);
  } while ((rc == LIBEVDEV_READ_STATUS_SUCCESS ||
            rc == LIBEVDEV_READ_STATUS_SYNC ||
            rc == -EAGAIN));

  // return
  return rc;
}

/*
 * kbdm_poll_cleanup
 *
 * close file descriptor
 * free memory allocated for thread
 */
static void
kbdm_poll_cleanup(void *arg) {
  // get kbd info pointer
  struct kbd_info **kbdip = arg;
  // get kbdi
  struct kbd_info *kbdi = *kbdip;
  // close fd
  close(kbdi->fd);
  // free memory
  libevdev_free(kbdi->ldev);
  free(kbdi->ev);
  free(kbdi);
}

static int
start_kbdm_polling(struct kbd_info **kbdi, const char *devname) {
  // init thread args
  init_thread_vars(kbdi, devname);

  // prepare polling
  if (prepare_polling(kbdi) < 0) {
    kbdmlogerr("cannot prepare polling", true);
    return -1;
  }

  // start polling
  if (start_polling(*kbdi) < 0) {
    kbdmlogerr("cannot poll for input", true);
    return -1;
  }

  return 0;
}

void * kbdm_poll_handle(void *args) {
  // get devname
  const char *devname = args;
  struct kbd_info *kbdi = NULL;

  // init thread state
  if (init_thread_state() < 0) {
    return NULL;
  }

  // push cleanup handle
  pthread_cleanup_push(kbdm_poll_cleanup, &kbdi);

  // start polling
  if (start_kbdm_polling(&kbdi, devname) < 0) {
    return NULL;
  }

  // pop cleanup handle
  pthread_cleanup_pop(1);

  // return
  return args;
}
