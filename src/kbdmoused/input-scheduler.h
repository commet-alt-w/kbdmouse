#ifndef INPUT_SCHEDULER_H
#define INPUT_SCHEDULER_H

/*
 * start_kbdm_input_scheduler
 *
 * initialize thread vars
 * initialize keyboard polling threads
 * run threads
 */
void start_kbdm_input_scheduler(void);

/*
 * kbdm_input_cleanup
 *
 * cancel threads, free memory, 
 * clear data segment memory
 * 
 */
void kbdm_input_cleanup(void);

#endif
