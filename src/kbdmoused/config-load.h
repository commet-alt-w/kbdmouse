#ifndef CONFIG_LOAD_H
#define CONFIG_LOAD_H

#include "config.h"

/*
 * load_config
 *
 * load config from config file into a kbdm_config_t
 *
 * path arg is an absolute path to config
 * config arg is a pointer to a kbdm_config_t
 *
 * return 0 on success
 * return -1 on failure
 */
int load_config(const char *path,
                kbdm_config_t *config);

#endif
