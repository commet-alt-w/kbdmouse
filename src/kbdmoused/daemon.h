#ifndef DAEMON_H
#define DAEMON_H

#include <sys/types.h>

/*
 * daemonize
 *
 * make the running process a daemon
 * using double fork and setsid
 *
 * return -1 on failure
 * return pid on success
 */
pid_t daemonize(void);

/*
 * change_signal_handle
 *
 * change signal handle for given signal to given handle
 *
 * return 0 on success
 * return -1 on error
 */
int change_signal_handle(int signal, void (*handle)(int));

/*
 * use pid file to prevent more daemons from running
 *
 * return -1 if it fails
 * return pidfile file descriptor on success
 */
int lock_daemon(const char * const pidfile,
                const int pidflags, const int pidmode);

#endif
