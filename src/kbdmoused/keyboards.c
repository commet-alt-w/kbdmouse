#include "keyboards.h"
#include "../kbdm.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <libudev.h>

/*
 * get_kbds
 *
 * internal use singleton used to store linked list
 *
 * return a pointer to a pointer to the first item in
 *   the linked list
 */
static keyboard_t **
get_kbds(void) {
  static keyboard_t *kbds;
  return &kbds;
}

/*
 * free_udev_device_get_next
 *
 * free memory for udev device, point udev list entry
 * to next item int he list,  and create a new udev
 * device from the next list entry.
 *
 * returns a pointer to the next udev device in the list
 */
static struct udev_device *
free_udev_device_get_next(struct udev *pudev,
                          struct udev_device *pudevd,
                          struct udev_list_entry **pudevle) {
  // init return
  struct udev_device *pudevd_new = NULL;
  // free memory
  udev_device_unref(pudevd);
  // get next list item
  *pudevle = udev_list_entry_get_next(*pudevle);
  // create new udev device
  const char *devicename = udev_list_entry_get_name(*pudevle);
  pudevd_new = udev_device_new_from_syspath(pudev, devicename);

  return pudevd_new;
}

/*
 * get_devname
 *
 * return udev_device DEVNAME property
 * return's null if it doesn't exist
 */
static const char *
get_devname(struct udev_device *pudevd) {
  // init return
  const char *devname = udev_device_get_property_value(pudevd,
                                                       DEVNPROPKEY);
  return devname;
}

/*
 * set_devname
 *
 * set keyboard_t devname
 *
 * return length of devname string on success
 * returns -1 on error
 */
static int
set_devname(const char *devname,
            keyboard_t *kbd) {
  int len = 0;
  // init devnames string store devname
  len = snprintf(NULL, 0, "%s", devname);
  kbd->devname = calloc(1, len+1);
  // store devname
  len = snprintf(kbd->devname, len+1, "%s", devname);
  // return length
  return len;
}

/*
 * init_next_kbd_item
 *
 * initialize the next keyboard_t linked list item
 * if there are more list entries to process. otherwise
 * set next to NULL (end of linked list)
 *
 * return's modified kbd
 */
static keyboard_t *
init_next_kbd_item(struct udev_list_entry *pudevle,
                   keyboard_t *kbd) {
  // check for more list itmes
  struct udev_list_entry *ule;
  ule = udev_list_entry_get_next(pudevle);
  if (ule) {
    // initialize next keyboard
    kbd->next = calloc(1, sizeof(keyboard_t));
    kbd = kbd->next;
  } else {
    // set next to null (end of linked list)
    kbd->next = NULL;
  }
  return kbd;
}

/*
 * check for keyboard
 *
 * check if udev_device is a keyboard
 *
 * return true if the udev device is a keyboard
 * return false otherwise
 */
static bool
is_keyboard(struct udev_device *pudevd) {
  // get ev value
  const char *ev = udev_device_get_property_value(pudevd,
                                                  EVPROPKEY);
  // check if ev value matches a keybaord
  if (ev && strncmp(MAGICKBDVAL, ev, MAGICKBDVALLEN) == 0) {
    return true;
  } else {
    return false;
  }
}

/*
 * add_kbd_item
 *
 * add a keyboard_t item to the linked list
 *
 * return pointer to next keyboard_t linked list item on success
 * return NULL on failure
 */
static keyboard_t *
add_kbd_item(struct udev_device *pudevd,
             struct udev_list_entry *pudevle,
             keyboard_t *kbd) {
  // get devname
  const char *devname = get_devname(pudevd);
  // set devname
  if (set_devname(devname, kbd) < 0) {
    return NULL;
  }
  // init next keyboard item
  kbd = init_next_kbd_item(pudevle, kbd);
  // return
  return kbd;
}

/*
 * search_for_keyboards
 *
 * iterate udev device list searching for keyboards
 *
 * returns first item of keyboard_t linked list on success
 * returns NULL on failure
 */
static keyboard_t *
search_for_keyboards(struct udev *pudev,
                     struct udev_list_entry *pudevle,
                     keyboard_t *kbd) {
  struct udev_list_entry *pudevle_next;
  keyboard_t *kbd_next = kbd;
  // iterate udev list entry
  udev_list_entry_foreach(pudevle_next, pudevle) {
    // create udev device
    const char *devicename = udev_list_entry_get_name(pudevle_next);
    struct udev_device *pudevd;
    pudevd = udev_device_new_from_syspath(pudev, devicename);
    // check if device is keyboard
    if(is_keyboard(pudevd)) {
      // next device in the list will have the devname we need
      pudevd = free_udev_device_get_next(pudev, pudevd, &pudevle_next);
      // add keyboard item to linked list
      kbd_next = add_kbd_item(pudevd, pudevle_next, kbd_next);
      // check for errors
      if (!kbd_next) {
        return NULL;
      }
    }
    // free memory
    udev_device_unref(pudevd);
  }
  return kbd;
}

/*
 * get_udev_keybaords
 *
 * get system keyboards
 *
 * returns first item in keyboard_t linked list on success
 * returns NULL on failure
 */
static keyboard_t *
get_udev_keyboards(struct udev *pudev,
             struct udev_enumerate *pudeve) {
  struct udev_list_entry *pudevle;
  keyboard_t *kbd = NULL;

  // get first list entry
  pudevle = udev_enumerate_get_list_entry(pudeve);
  // init keyboard
  kbd = calloc(1, sizeof(keyboard_t));
  // search for keyboards
  kbd = search_for_keyboards(pudev, pudevle, kbd);

  return kbd;
}

int get_num_keyboards() {
  int num_kbds = 0;
  keyboard_t *kbd_n;

  for (kbd_n = *get_kbds(); kbd_n != NULL; kbd_n = kbd_n->next) {
    num_kbds++;
  }

  return num_kbds;
}

const keyboard_t *
get_keyboards(void) {
  int enumr = 0;
  struct udev *pudev;
  struct udev_enumerate *pudeve;
  keyboard_t **kbd = get_kbds();

  // init udev
  pudev = udev_new();
  // init udev enumeration
  pudeve = udev_enumerate_new(pudev);
  // add property filter
  enumr = udev_enumerate_add_match_property(pudeve,
                                            KBDPROPKEY,
                                            KBDPROPVAL);
  // check add filter result
  if (enumr > -1) {
    // scan for devices
    udev_enumerate_scan_devices(pudeve);
    // get pointer to first keyboard
    *kbd = get_udev_keyboards(pudev, pudeve);
    // check for errors
    if (!kbd) {
      return NULL;
    }
  }

  // free memory
  udev_enumerate_unref(pudeve);
  udev_unref(pudev);

  // cast to const and return
  return (const keyboard_t *)*kbd;
}

void cleanup_keyboards(void) {
  // get kbd pointer
  keyboard_t **kbd = get_kbds();
  keyboard_t *kbd_n;
  // iterate kbds
  for (kbd_n = *kbd; kbd_n != NULL;) {
    // store kbd we want to delete
    keyboard_t *kbd_d = kbd_n;
    // set next
    kbd_n = kbd_n->next;
    // free memory
    free(kbd_d->devname);
    free(kbd_d);
  }
}
