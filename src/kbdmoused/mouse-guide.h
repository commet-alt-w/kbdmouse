#ifndef MOUSE_GUIDE_H
#define MOUSE_GUIDE_H

/*
 * kbdm_mouse_handle
 *
 * mouse movement thread handle
 */
void * kbdm_mouse_guide_handle(void *args);

#endif
