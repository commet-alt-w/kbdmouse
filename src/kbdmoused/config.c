#include "config.h"
#include "config-load.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>
#include <fcntl.h>
#include <libgen.h>

// static to store config file absolute path
static char *config_abs_path;

/*
 * get_kbdm_config
 *
 * singleton to store config data
 */
static kbdm_config_t **
get_kbdm_config(void) {
  static kbdm_config_t *config = NULL;
  return &config;
}

/*
 * init_config_abs_path
 *
 * initialize config_abs_path with the provided
 * path and file args. allocate heap memory
 *
 * path should be a folder where file resides
 */
static void
init_config_abs_path(const char *path,
                     const char *file) {
  // get length of final string
  int len;
  len = snprintf(NULL, 0, "%s/%s", path, file);
  // allocate memory
  config_abs_path = calloc(1, len+1);
  // copy path
  snprintf(config_abs_path, len+1, "%s/%s", path, file);
}

/*
 * get_config_abs_path
 *
 * initialize config_abs_path if not initialized
 *
 * return config_abs_path pointer
 */
static const char *
get_config_abs_path(void) {
  // check if config path was initialized
  if (!config_abs_path) {
    init_config_abs_path(CONFIG_PATH, CONFIG_FILE);
  }
  // cast to const and return
  return (const char *) config_abs_path;
}

/*
 * init_config
 *
 * intialize config
 */
static int
init_kbdm_config(void) {
  // init return
  int res = 0;
  // get config pointer
  kbdm_config_t **config = get_kbdm_config();
  // allocate memory
  *config = calloc(1, sizeof(kbdm_config_t));
  // get path
  const char *path = get_config_abs_path();
  // load config
  res = load_config(path, *config);
  // return result
  return res;
}

/*
 * init_kbdm_config_file
 *
 * initialize config_abs_path to provided path
 *
 * path arg is an absolute file path
 *
 * return 0 on success
 * return -1 on failure
 */
static int
init_kbdm_config_file(const char *path) {
  // init return
  int ret = 0;
  // get dirname/basename
  char *dirbuf = strndup(path, PATH_MAX);
  char *basebuf = strndup(path, PATH_MAX);
  char *dir = dirname(dirbuf);
  char *base = basename(basebuf);
  // check for errors
  if (!dir || strncmp(base, ".", 2) == 0) {
    // set error return val
    ret = -1;
  } else {
    // init config absolute path
    init_config_abs_path(dir, base);
  }
  // free memory
  free(dirbuf);
  free(basebuf);
  // return
  return ret;
}

int init_config_file(const char *path) {
  // init return
  int ret = -1;
  // get realpath
  char *buf = realpath(path, NULL);
  // check for errors
  if (!buf) {
    // return error
    return ret;
  }
  // init path
  ret = init_kbdm_config_file(buf);
  // free memory
  free(buf);
  // return success
  return ret;
}

bool config_present(void) {
  // get config file
  const char *conff = get_config_abs_path();
  // attempt to open config file
  int fd = open(conff, CONFIG_FLAGS);
  // check for error
  if (fd < 0) {
    return false;
  } else {
    close(fd);
    return true;
  }
}

void cleanup_config(void) {
  // get config
  kbdm_config_t **config = get_kbdm_config();
  // free memory
  free(*config);
  free(config_abs_path);
  // nullify
  *config = NULL;
  config = NULL;
  config_abs_path = NULL;
}

int init_config(void) {
  // init return
  int res = -1;
  // get config
  kbdm_config_t *config = *get_kbdm_config();
  // check if we have to initialze
  if (!config) {
    // init config
    res = init_kbdm_config();
  }
  // return
  return res;
}

const kbdm_config_t * get_config(void) {
  // get config
  kbdm_config_t *config = *get_kbdm_config();
  // cast to const and return
  return (const kbdm_config_t *)config;
}
