#ifndef EVENT_H
#define EVENT_H

#include <libevdev/libevdev.h>

// kbdm event struct
typedef struct kbdm_event {
  struct input_event *ev;
} kbdm_event_t;

// kbdm event queue item
struct event_q_item {
  kbdm_event_t *event;
  struct event_q_item *next;
};

/*
 * kbdm_event_init
 *
 * initialize kbdm event queue
 */
void kbdm_event_init(void);

/*
 * kbdm_event_cleanup
 *
 * free memory for any kbdm_event_t queue items
 * if the program is terminated before delivering them
 */
void kbdm_event_cleanup(void);

/*
 * send_event
 *
 * push event into event queue
 */
void push_event(kbdm_event_t *e);

/*
 * get_event
 *
 * retrieve an event from event queue
 */
kbdm_event_t * pull_event(void);

#endif
