#ifndef CONFIG_H
#define CONFIG_H

#include <stdbool.h>
#include <stdatomic.h>

#define CONFIG_PATH "/etc/kbdmouse"
#define CONFIG_FILE "kbdm.conf"
#define CONFIG_FLAGS O_RDONLY

#define CONF_BUFF_SIZE 128
#define CONF_MOD_DELIM "+"
#define CONF_MOUSESENS "mouse_sensitivity"
#define CONF_MODARROWS "modify_arrows"
#define CONF_MODIFIERS "modifiers"
#define CONF_TOGGLE    "toggle"
#define CONF_LMOUSE    "left_mouse"
#define CONF_RMOUSE    "right_mouse"
#define CONF_SCROLLU   "scroll_up"
#define CONF_SCROLLD   "scroll_down"
#define CONF_UP        "up"
#define CONF_RIGHT     "right"
#define CONF_DOWN      "down"
#define CONF_LEFT      "left"

// custom type to store keybindings
typedef struct keybinds {
  atomic_int *modifiers;
  atomic_int toggle;
  atomic_int left_mouse;
  atomic_int right_mouse;
  atomic_int scroll_up;
  atomic_int scroll_down;
  atomic_int up;
  atomic_int right;
  atomic_int down;
  atomic_int left;
} keybinds_t;

// custom type to store config
typedef struct kbdm_config {
  keybinds_t keys;
  _Atomic float  mouse_sensitivity;
  atomic_bool modify_arrows;
} kbdm_config_t;

/*
 * init_config
 *
 * initialize kbdm_config_t
 *
 * return 0 on success
 * return -1 on failure
 */
int init_config(void);

/*
 * get_config
 *
 * return a pointer to config
 * return NULL on failure
 */
const kbdm_config_t * get_config(void);

/*
 * cleanup_config
 *
 * free memory from config allocation
 */
void cleanup_config(void);

/*
 * config_present
 *
 * return true if config is present
 * return false otherwise
 */
bool config_present(void);

/*
 *
 * init_config_file
 *
 * use a custom location for the config file
 *
 * path arg is the absolute or relative file path
 * for the config file
 *
 * return 0 on success
 * return -1 on failure
 */
int init_config_file(const char *path);

#endif
