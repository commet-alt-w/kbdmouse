#ifndef KEYBOARDS_H
#define KEYBOARDS_H

// libudev filtering constants
#define SUBSYS "input"
#define KBDPROPKEY "ID_INPUT_KEYBOARD"
#define KBDPROPVAL "1"
#define DEVNPROPKEY "DEVNAME"
#define EVPROPKEY "EV"
#define MAGICKBDVAL "120013"
#define MAGICKBDVALLEN 6

// custom keyboard type
typedef struct keyboard {
  char *devname;
  struct keyboard *next;
} keyboard_t;

/*
 * get_keyboards
 *
 * use libudev to build a linked list of
 * keyboard dev paths
 *
 * return a pointer to the first keyboard
 * return NULL on failure
 */
const keyboard_t * get_keyboards(void);

/*
 * cleanup_keyboards
 *
 * free memory allocated to keyboard_t linked list
 */
void cleanup_keyboards(void);

/*
 * get_num_kbds
 *
 * return the number of keyboards in the keyboard_t
 * linked list
 */
int get_num_keyboards(void);

#endif
