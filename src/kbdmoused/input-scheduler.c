#include "input-scheduler.h"
#include "keyboards.h"
#include "scheduler.h"
#include "poll.h"
#include "../kbdm.h"
#include <stdlib.h>
#include <pthread.h>

// thread vars
static const keyboard_t *kbds;
static int num_kbds;

/*
 * get_pthreads
 *
 * singleton to store thread info
 *
 * return a pointer to thread_info array
 */
static struct thread_info **
get_pthreads(void) {
  static struct thread_info *pthreads;
  return &pthreads;
}

/*
 * init_vars
 *
 * get keyboard_t linked list of keyboards
 * and number of keyboards
 */
static void
init_vars(void) {
  // get keyboards
  kbds = get_keyboards();
  // store number of keyboards
  num_kbds = get_num_keyboards();
  // allocate memory for pthreads
  struct thread_info **pthreads = get_pthreads();
  *pthreads = calloc(num_kbds, sizeof(struct thread_info));
}

/*
 * init_thread_vars
 *
 * initialize keyboard polling threads
 */
static void
init_threads(void) {
  // get pthreads
  struct thread_info *pthreads = *get_pthreads();
  // init pthreads
  for (int i = 0; i < num_kbds; i++) {
    pthreads[i].thread_num = i+1;
    pthreads[i].handle = kbdm_poll_handle;
  }
}

/*
 * start_threads
 *
 * start keyboard polling threads, pass /dev/input/event*
 * device to polling thread
 */
static void
start_threads(void) {
  // init keyboard
  const keyboard_t *kbdn = kbds;
  // get pthreads
  struct thread_info *pthreads = *get_pthreads();
  // iterate pthreads
  for (int i = 0; i < num_kbds; i++) {
    // start thread
    pthread_create(&pthreads[i].thread_id, NULL,
                   pthreads[i].handle, kbdn->devname);
    // set next keyboard
    kbdn = kbdn->next;
  }
}

void kbdm_input_cleanup(void) {
  // get threads
  struct thread_info *pthreads = *get_pthreads();
  // iterate threads
  for (int i = 0; i < num_kbds; i++) {
    // cancel thread
    pthread_cancel(pthreads[i].thread_id);
    // join thread waiting for cancel
    pthread_join(pthreads[i].thread_id, NULL);
  }
  // free memory
  free(pthreads);
  cleanup_keyboards();
  // clear data segment memory
  num_kbds = 0;
}

void start_kbdm_input_scheduler(void) {
  // init thread vars
  init_vars();
  
  // init keyboard input threads
  init_threads();

  // start threads
  start_threads();
}
