#include "event.h"
#include "scheduler.h"
#include "../kbdm.h"
#include <stdlib.h>
#include <stdbool.h>
#include <stdatomic.h>
#include <unistd.h>
#include <pthread.h>

static struct event_q_item *head;
static struct event_q_item *tail;
static atomic_bool is_empty;

/*
 * queue_is_empty
 *
 * the queue is defined as empty when head is NULL
 *
 * return true if the queue is empty
 * return false otherwise
 */
static bool
queue_is_empty(void) {
  return is_empty;
}

/*
 * add_to_tail
 *
 * add an item to the end of the queue
 */
static void
add_to_tail(struct event_q_item *e) {
  // check if tail was set
  if (tail == NULL) {
    head->next = e;
    tail = e;
  } else {
    tail->next = e;
    tail = e;
  }
}

/*
 * add_to_head
 *
 * add an item to the head of the queue
 * only used if the queue is empty
 */
static void
add_to_head(struct event_q_item *e) {
  head = e;
}

/*
 * remove_head
 *
 * remove first item from the queue
 *
 * returns first kbdm_event_t in the queue
 */
static struct event_q_item *
remove_head(void) {
  // get head
  struct event_q_item *item = head;
  // check if there is another item in the queue
  if (head->next != NULL) {
    // mark next item as start of the queue
    head = head->next;
  } else {
    // list is now empty
    head = NULL;
    tail = NULL;
    // update is_empty
    is_empty = true;
  }

  // return
  return item;
}

/*
 * queue_event
 *
 * and a kbdm_event_t item to the queue
 */
static void
queue_event(struct event_q_item *e) {
  // check if queue is empty
  if (queue_is_empty()) {
    // add to head
    add_to_head(e);
    // update is empty
    is_empty = false;
  } else {
    // list not empty, add to tail
    add_to_tail(e);
  }
}

/*
 * get_event
 *
 * wait for event to be queue
 *
 * return first kbdm_event_t item in the queue
 */
static struct event_q_item *
get_next_event(void) {
  // init return
  struct event_q_item *e = NULL;
  
  // check if queue is empty
  if (queue_is_empty()) {
    DKBDMLOGINFO("event queue is empty");
    // wait for event
    while (queue_is_empty()) {
      // sleep to prevent high cpu usage
      usleep(MICROSECSLEEP);
    }
    DKBDMLOGINFO("event queue recieved event");
  }

  // get first item in queue
  e = remove_head();
  
  return e;
}

static struct event_q_item *
new_event_q_item(kbdm_event_t *e) {
  // init return
  struct event_q_item *item = NULL;
  item = calloc(1, sizeof(struct event_q_item));
  // add kbdm event to q item
  item->event = e;
  // return
  return item;
}

void kbdm_event_init(void) {
  head = NULL;
  tail = NULL;
  is_empty = true;
}

void kbdm_event_cleanup() {
  // check if queue is empty
  if (!queue_is_empty()) {
    struct event_q_item *e;
    // iterate queue to delete elements
    for (e = head; e != NULL;) {
      // set element we want to delete
      struct event_q_item *d = e;
      // set next
      e = e->next;
      // free memory
      free(d->event->ev);
      free(d->event);
      free(d);
    }
  }
}

void push_event(kbdm_event_t *e) {
  // init q item
  struct event_q_item *item = new_event_q_item(e);
  // add to q
  queue_event(item);
}

kbdm_event_t *pull_event(void) {
  // init return
  kbdm_event_t *e = NULL;
  // get next event in queue
  struct event_q_item *item = get_next_event();
  // get kbdm event from event q item
  e = item->event;
  // free memory
  free(item);
  // return
  return e;
}
