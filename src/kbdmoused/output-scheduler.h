#ifndef OUTPUT_SCHEDULER_H
#define OUTPUT_SCHEDULER_H

#include "cursor-move.h"

// macro constants
#define NUM_CURSOR_MOVE_THREADS 4
#define NUM_MOUSE_GUIDE_THREADS 1
#define DIRECTIONS (int[]){UP, RIGHT, DOWN, LEFT}

/*
 * start_kbdm_output_scheduler
 *
 * start scheduler for mouse movement control
 * writing to /dev/uinput
 */
void start_kbdm_output_scheduler(void);

/*
 * kbdm_output_cleanup
 *
 * cancel threads and free memory
 */
void kbdm_output_cleanup(void);

#endif
