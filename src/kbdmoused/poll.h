#ifndef POLL_H
#define POLL_H

// file open flags
#define FFLAGS O_RDONLY
// libevdev polling flags
#define LDEVFLAGS LIBEVDEV_READ_FLAG_NORMAL|LIBEVDEV_READ_FLAG_BLOCKING

// keyboard info struct
struct kbd_info {
  struct libevdev *ldev;
  struct input_event *ev;
  int fd;
  const char *devname;
};

/*
 * kbdm_poll_handle
 *
 * handle for kbdm poll thread
 *
 * args must be a /dev/input/event* device devname
 */
void * kbdm_poll_handle(void *args);

#endif
