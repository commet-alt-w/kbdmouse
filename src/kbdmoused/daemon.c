#include "daemon.h"
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/file.h>
#include <sys/stat.h>

/*
 * write_lock_pidfile
 *
 * write lock the pid file
 *
 * return 0 on success
 * return -1 otherwise
 */
static int
write_lock_pidfile(int fd) {
  struct flock fl;
  // write lock pid file
  fl.l_type = F_WRLCK;
  fl.l_start = 0;
  fl.l_whence = SEEK_SET;
  fl.l_len = 0;

  int fcntlr = fcntl(fd, F_SETLK, &fl);

  // check for errors
  if (fcntlr < 0) {
    return -1;
  }

  return 0;
}

/*
 * write_pid
 *
 * write pid to pidfile
 *
 * return 0 on success
 * return -1 otherwise
 */
static
int write_pid(int fd) {
  // write process pid to pid file
  pid_t pid = getpid();
  int len = snprintf(NULL, 0, "%ld", (long)pid);
  char * const buffer = calloc(len+1, sizeof(char));

  if (snprintf(buffer, len+1, "%ld", (long)pid) > 0) {
    if (write(fd, buffer, len+1) <= 0) {
      return -1;
    }
  } else {
    return -1;
  }
  
  free(buffer);

  return 0;
}

/*
 * send_std_fds
 *
 * send STDIN, STDOUT, STDERR to /dev/null
 *
 * return 0 on success
 * return -1 otherwise
 */
static
int send_std_fds(void) {
  int fd0 = open("/dev/null", O_RDONLY, 0666);
  int fd1 = open("/dev/null", O_WRONLY, 0666);
  int fd2 = open("/dev/null", O_RDWR, 0666);

  if (fd0 < 0 || fd1 < 0 || fd2 < 0) {
    return -1;
  }

  dup2(fd0, 0);
  dup2(fd1, 1);
  dup2(fd2, 2);

  close(fd0);
  close(fd1);
  close(fd2);

  return 0;
}

/*
 * change_sighup_handle
 *
 * set function to handle sighup
 *
 * return 0 on success
 * return -1 otherwise
 */
static int
change_sighup_handle(void (*handle)(int)) {
  struct sigaction sa;
  sa.sa_handler = handle;
  // initialize signal set to empty
  // and exclude signals (set to block all signals)
  sigemptyset(&sa.sa_mask);
  // no flag
  sa.sa_flags = 0;

  // ignore sighup
  if (sigaction(SIGHUP, &sa, NULL) < 0) {
    return -1;
  }

  return 0;
}

/*
 * fork_parent_exit
 *
 * fork and let parent process exit
 *
 * return pid on success
 * return -1 otherwise
 */
static pid_t
fork_exit_parent(void) {
  // fork from parent
  pid_t pid = fork();

  // check for errors
  if (pid < 0) {
    return -1;
  } else if (pid != 0) {
    // let parent exit
    exit(EXIT_SUCCESS);
  }
  
  return pid;
}

/*
 * double_fork
 *
 * perform double fork, detaching from controlling
 * terminal. remove session/group leader status 
 * preventing future acquisition by controlling
 * terminal. 
 *
 * return pid on success
 * return -1 otherwise
 */
static int
double_fork(void) {
  pid_t pid;
  // fork
  pid = fork_exit_parent();

  if (pid < 0) {
    return -1;
  }
  
  // set session leader
  setsid();

  // ignore sighup (being disconnected from controlling terminal)
  if (change_sighup_handle(SIG_IGN) < 0) {
    return -1;
  }

  // fork again to prevent aquiring controlling terminal
  pid = fork_exit_parent();

  if (pid < 0) {
    return -1;
  }

  // restore sighup default handle
  if (change_sighup_handle(SIG_DFL) < 0) {
    return -1;
  }

  return pid;
}

int lock_daemon(const char * const pidfile,
                const int pidflags, const int pidmode) {
  int fd = -1;

  // open/create pid file
  fd = open(pidfile, pidflags, pidmode);

  // check for io errors
  if (fd < 0) {
    return -1;
  }

  // write lock pidfile
  if (write_lock_pidfile(fd) < 0) {
    close(fd);
    return -1;
  }
  
  // write pid to pid file
  if (write_pid(fd) < 0) {
    return -1;
  }
  
  // return file descriptor
  return fd;
}

int change_signal_handle(int signal, void (*handle)(int)) {
  struct sigaction sa;
  sa.sa_handler = handle;
  // initialize signal set to empty
  // and exclude signals (set to block all signals)
  sigemptyset(&sa.sa_mask);
  sigfillset(&sa.sa_mask);
  // no flag
  sa.sa_flags = 0;

  if (sigaction(signal, &sa, NULL) < 0) {
    return -1;
  }

  return 0;
}

pid_t daemonize(void) {
  pid_t pid;

  // clear create file mask
  umask(0);

  // double fork
  pid = double_fork();

  // check for errors
  if (pid < 0) {
    return -1;
  }

  //change cwd to root
  if (chdir("/") < 0) {
    return -1;
  }

  // connect stdin, stdout, stderr to /dev/null
  if (send_std_fds() < 0) {
    return -1;
  }

  return pid;
}
