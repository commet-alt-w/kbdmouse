#ifndef CURSOR_MOVE_H
#define CURSOR_MOVE_H

#include <pthread.h>
#include <stdatomic.h>

// macro constants
#define CURSOR_MOVE_DELTA 5

// directions enum
enum direction {
  UP    = 0,
  RIGHT = 1,
  DOWN  = 2,
  LEFT  = 3
};

// x/y axis enum
enum axis {
  X_AXIS, Y_AXIS
};

// key press state enum
enum key_press_state {
  PRESSED, RELEASED
};

// custome type for cursor info
typedef struct cursor_info {
  //const pthread_mutex_t *move_mutex;
  const struct libevdev_uinput *uidev;
  atomic_int *key_state;
  int direction;
} cursor_info_t;

/*
 * kbdm_cursor_handle
 */
void * kbdm_cursor_handle(void *args);

#endif
