#ifndef KBDMOUSED_SCHEDULER_H
#define KBDMOUSED_SCHEDULER_H

#include <pthread.h>

// 0.01 seconds
#define MICROSECSLEEP 10000

// thread info struct
struct thread_info {
  pthread_t  thread_id;
  int        thread_num;
  void * (*handle)(void *);
};

/*
 * init_thread_state
 *
 * set pthread cancel state and type
 *
 * return 0 on success
 * return -1 on failure
 */
int init_thread_state(void);
  
/*
 * start_kbdm_scheduler
 *
 * start scheduler and related thread
 * management
 *
 * return 0 on success
 * return -1 on failure
 */
int start_kbdm_scheduler(void);

/*
 * scheduler_cleanup
 *
 * cancel running threads and free memory
 */
void scheduler_cleanup(void);

#endif
