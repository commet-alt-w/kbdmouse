#include "output-scheduler.h"
#include "scheduler.h"
#include "mouse-guide.h"
#include "mouse.h"
#include "cursor-move.h"
#include "../kbdm.h"
#include <stdlib.h>
#include <stdatomic.h>
#include <pthread.h>
#include <libevdev/libevdev-uinput.h>

static cursor_info_t *cursor_vars;
static const struct libevdev_uinput *uidev;
static const int next_thread = NUM_CURSOR_MOVE_THREADS;

/*
 * get_pthreads
 *
 * singleton to store thread info
 *
 * return a pointer to thread_info array
 */
static struct thread_info **
get_pthreads(void) {
  static struct thread_info *pthreads;
  return &pthreads;
}

/*
 * num_threads
 *
 * get total number of threads
 */
static int
num_threads(void) {
  // add up total number of threads
  int num_threads =
    NUM_MOUSE_GUIDE_THREADS +
    NUM_CURSOR_MOVE_THREADS;
  // return count
  return num_threads;
}

/*
 * init_vars
 *
 */
static void
init_vars(void) {
  // get libevdev uinput device (virtual mouse)
  uidev = get_mouse();
  // get pthreads
  struct thread_info **pthreads = get_pthreads();
  // init pthreads
  *pthreads = calloc(num_threads(), sizeof(struct thread_info));
  // init cursor vars
  cursor_vars = calloc(NUM_CURSOR_MOVE_THREADS,
                       sizeof(cursor_info_t));
  for (int i = 0; i < NUM_CURSOR_MOVE_THREADS; i++) {
    // set direction
    cursor_vars[i].direction = DIRECTIONS[i];
    // allocate memory for key state pointer
    cursor_vars[i].key_state = calloc(1, sizeof(atomic_int));
    // set initial value of key state
    *cursor_vars[i].key_state = RELEASED;
    // set uidev
    cursor_vars[i].uidev = uidev;
  }
}

/*
 * init_thread_vars
 *
 */
static void
init_threads(void) {
  // get pthreads array
  struct thread_info *pthreads = *get_pthreads();
  // init moust-control thread
  pthreads[next_thread].thread_num = 1;
  pthreads[next_thread].handle = kbdm_mouse_guide_handle;
  // init cursor-move threads
  for (int i = 0; i < NUM_CURSOR_MOVE_THREADS; i++) {
    // set thread number
    pthreads[i].thread_num = i+1;
    // set handle
    pthreads[i].handle = kbdm_cursor_handle;
  }
}

/*
 * start_threads
 *
 */
static void
start_threads(void) {
  // get pthreads array
  struct thread_info *pthreads = *get_pthreads();
  // start mouse-control thread
  pthread_create(&pthreads[next_thread].thread_id, NULL,
                 pthreads[next_thread].handle, cursor_vars);
  // start cursor-move threads
  for (int i = 0; i < NUM_CURSOR_MOVE_THREADS; i++) {
    // start thread
    pthread_create(&pthreads[i].thread_id, NULL,
                   pthreads[i].handle, &cursor_vars[i]);
  }
}

void kbdm_output_cleanup(void) {
  // get pthreads array
  struct thread_info *pthreads = *get_pthreads();
  // cancel mouse-control thread
  pthread_cancel(pthreads[next_thread].thread_id);
  // join waiting for cancel
  pthread_join(pthreads[next_thread].thread_id, NULL);
  // iterate pthreads and cursor vars
  for (int i = 0; i < NUM_CURSOR_MOVE_THREADS; i++) {
    // cancel thread
    pthread_cancel(pthreads[i].thread_id);
    // join waiting for cancel
    pthread_join(pthreads[i].thread_id, NULL);
    // free memory for cursor vars
    free(cursor_vars[i].key_state);
  }
  // free cursor vars
  free(cursor_vars);
  // free memory
  free(pthreads);
  // cleanup libevdev uinput device
  cleanup_mouse();
}

void start_kbdm_output_scheduler(void) {
  // init thread vars
  init_vars();
  // init threads
  init_threads();
  // start threads
  start_threads();
}
