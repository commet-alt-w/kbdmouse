#include "cursor-move.h"
#include "scheduler.h"
#include "../kbdm.h"
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <libevdev/libevdev-uinput.h>

/*
 * cursor_move_err_check
 *
 * log any errors from writing to /dev/uinput
 */
static void
cursor_move_err_check(int err) {
  if (err != 0) {
    // log error
    kbdmlogerr("error moving cursor", true);
  }
}

/*
 * move_cursor
 *
 * move cursor along axis with delta by
 * writing to libevdev uinput device, and
 * report event
 */
static void
move_cursor(int axis, int delta,
            const struct libevdev_uinput *uidev) {
  // init error state
  int err = 0;
  // check axis
  if (axis == X_AXIS) {
    // write to uinput device
    err = libevdev_uinput_write_event(uidev, EV_REL,
                                      REL_X, delta);
  } else if (axis == Y_AXIS) {
    // write to uinput device
    err = libevdev_uinput_write_event(uidev, EV_REL,
                                      REL_Y, delta);
  }
  // check for errors
  cursor_move_err_check(err);
  // report event
  err = libevdev_uinput_write_event(uidev, EV_SYN,
                                    SYN_REPORT, 0);
  // check for errors
  cursor_move_err_check(err);
}

/*
 * process_keystate
 *
 * write events and their reports to libevdev uinput
 * device based on the status of key_state
 */
static void
process_keystate(int axis, int delta, const cursor_info_t *civ) {
  // loop until thread is canceled
  while (true) {
    // wait for pressed key state
    while (*civ->key_state == PRESSED) {
      // move cursor
      move_cursor(axis, delta, civ->uidev);
      // sleep here adjusts mouse sensitivty
      usleep(9000);
    }
    // sleep to prevent high cpu usage
    usleep(MICROSECSLEEP);
  }
}

/*
 * start_kbdm_cursor_handle
 *
 * set target axis (x/y)
 * set delta according to axis
 * begin cursor movement loop
 */
static int
start_kbdm_cursor_handle(const cursor_info_t *civ) {
  int target_axis = 0;
  int move_delta = 0;

  // set axis/delta based on direction
  switch(civ->direction) {
  case UP:
    target_axis = Y_AXIS;
    move_delta = -CURSOR_MOVE_DELTA;
    break;
  case RIGHT:
    target_axis = X_AXIS;
    move_delta = CURSOR_MOVE_DELTA;
    break;
  case DOWN:
    target_axis = Y_AXIS;
    move_delta = CURSOR_MOVE_DELTA;
    break;
  case LEFT:
    target_axis = X_AXIS;
    move_delta = -CURSOR_MOVE_DELTA;
    break;
  }

  // begin processing keystate
  process_keystate(target_axis, move_delta, civ);
  
  return 0;
}

void * kbdm_cursor_handle(void *args) {
  // get cursor vars
  const cursor_info_t *cursor_vars = args;

  // init thread state
  init_thread_state();
 
  // get events
  if (start_kbdm_cursor_handle(cursor_vars) < 0) {
    return NULL;
  }
 
  return args;
}
