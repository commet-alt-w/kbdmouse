#include "mouse.h"
#include "../kbdm.h"
#include <stdlib.h>
#include <libevdev/libevdev-uinput.h>

static struct libevdev *dev;
static struct libevdev_uinput *uidev;

/*
 * create_libevdev_uinput_device
 *
 * initialize libevdev uinput device for
 * writing mouse movement events to.
 *
 * return 0 on success
 * return -1 on failure
 */
static int
create_libevdev_uinput_device(void) {
  int err = 0;

  // init libevdev uinput device
  err = libevdev_uinput_create_from_device(dev, UINPUT_FD, &uidev);

  // check for errors
  if (err != 0) {
    kbdmlogerr("cannot create libevdev uinput device", true);
    return -1;
  }

  return 0;
}

/*
 * create_libevdev_mouse
 *
 * create a virtual mouse to send events with
 * which will move the on-screen cursor.
 *
 * return 0 on success
 * return -1 on failure
 */
static int
create_libevdev_mouse(void) {
  // init libevdev device
  dev = libevdev_new();

  // check for errors
  if (!dev) {
    kbdmlogerr("cannot create virtual mouse", true);
    return -1;
  }

  // create virtual mouse
  libevdev_set_name(dev, DEVICE_NAME);
  // define x/y axis cursor movement events
  libevdev_enable_event_type(dev, EV_REL);
  libevdev_enable_event_code(dev, EV_REL, REL_X, NULL);
  libevdev_enable_event_code(dev, EV_REL, REL_Y, NULL);
  // define at least one mouse button even tho it's not used.
  // this finalizes the "mouse" portion of the virtual device
  libevdev_enable_event_type(dev, EV_KEY);
  libevdev_enable_event_code(dev, EV_KEY, BTN_LEFT, NULL);

  return 0;
}

/*
 * create mouse
 *
 * initialize libevdev virtual device (mouse)
 * initialize libevdev uinput device to write to
 *
 * return 0 on success
 * return -1 on failure
 */
static int
create_mouse(void) {
  // create libevdev mouse
  if (create_libevdev_mouse() < 0) {
    return -1;
  }

  // create libevdev uinput device
  if (create_libevdev_uinput_device() < 0) {
    return -1;
  }

  return 0;
}

const struct libevdev_uinput * get_mouse(void) {
  // check if libevdev device was initialized
  if (!uidev) {
    // create libevdev virtual device
    if (create_mouse() < 0) {
      return NULL;
    }
  }
  // cast uidev to const and return
  return (const struct libevdev_uinput *)uidev;
}

void cleanup_mouse(void) {
  // free memory
  libevdev_uinput_destroy(uidev);
  libevdev_free(dev);
}
