#include "scheduler.h"
#include "input-scheduler.h"
#include "output-scheduler.h"
#include "event.h"
#include "../kbdm.h"
#include <stdlib.h>
#include <syslog.h>

int init_thread_state(void) {
  int r = -1;
  
  // set cancel state
  r = pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
  // check for error
  if (r < 0) {
    return -1;
  }
  
  // set cancel type
  r = pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
  // check for errors
  if (r < 0) {
    return -1;
  }

  return 0;
}

void scheduler_cleanup(void) {
  // free memory
  kbdm_input_cleanup();
  kbdm_output_cleanup();
  kbdm_event_cleanup();
}

int start_kbdm_scheduler(void) {
  // init event handling queue
  kbdm_event_init();

  // start input scheduler
  start_kbdm_input_scheduler();

  // start output scheduler
  start_kbdm_output_scheduler();

  return 0;
}
