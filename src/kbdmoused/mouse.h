#ifndef MOUSE_H
#define MOUSE_H

// macro constants for libevdev uinput device
#define DEVICE_NAME "KBDM mouse"
#define UINPUT_FD LIBEVDEV_UINPUT_OPEN_MANAGED

/*
 * get_mouse
 *
 * return libevdev uinput device on success
 * return NULL on failure
 */
const struct libevdev_uinput * get_mouse(void);

/*
 * cleanup_mouse
 *
 * free libevdev device memory
 */
void cleanup_mouse(void);

#endif
