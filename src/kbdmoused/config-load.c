#include "config-load.h"
#include "config.h"
#include <stdlib.h>
#include <string.h>
#include <libconfig.h>

static int
count_delims(const char *str) {
  // init delimiter count
  int delim_count = 0;
  // get length
  int len = strnlen(str, CONF_BUFF_SIZE);
  // iterate char array
  for (int i = 0; i < len; i++) {
    // count delimiters
    if (strncmp(&str[i], CONF_MOD_DELIM, 1) == 0) {
      // increment count
      delim_count++;
    }
  }
  // return count
  return delim_count;
}

static int *
get_modifiers(const char *str) {
  // setup return array
  int *modifiers = NULL;
  int delims = count_delims(str);
  
  return modifiers;
}

static int
set_modifiers(const config_t *conf,
              kbdm_config_t *config) {
  // init result
  int res = 0;
  // init buffer
  const char *buf;
  // get modifiers string
  res = config_lookup_string(conf, CONF_MODIFIERS, &buf);
  // check for errors
  if (res == CONFIG_FALSE) {
    // return error
    return -1;
  }
  // get modifiers array
  int *arr = get_modifiers(buf);
  //config->keys.modifiers = arr;
  // return success
  return 0;
}

static int
process_keybindings(const config_t *conf,
                    kbdm_config_t *config) {
  // set keybindings
  if (set_modifiers(conf, config) < 0) {
    // return error
    return -1;
  }
  return 0;
}

static int
set_mouse_sensitivity(const config_t *conf,
                      kbdm_config_t *config) {
  // init result
  int res = 0;
  // tmp values
  double tmp_double = 0;
  // get mouse sensitivity
  res = config_lookup_float(conf, CONF_MOUSESENS, &tmp_double);
  // check for errors
  if (res == CONFIG_FALSE) {
    return -1;
  }
  // set mouse sensitivity
  config->mouse_sensitivity = tmp_double;
  
  return 0;
}

static int
set_modify_arrows(const config_t *conf,
                  kbdm_config_t *config) { 
  // init result
  int res = 0;
  // tmp values
  bool tmp_bool = false;
  int tmp_int = 0;
  // get modify arrows
  res = config_lookup_bool(conf, CONF_MODARROWS, &tmp_int);
  // check for errors
  if (res == CONFIG_FALSE) {
    return -1;
  }
  // set modify arrows
  tmp_bool = tmp_int;
  config->modify_arrows = tmp_bool;
  // return success
  return 0;
}

static int
process_general_settings(const config_t *conf,
                         kbdm_config_t *config) {
  // set general settings
  if (set_mouse_sensitivity(conf, config) < 0 ||
      set_modify_arrows(conf, config) < 0) {
    // return error
    return -1;
  }
  // return success
  return 0;
}

static int
process_kbdm_config(const config_t *conf,
                    kbdm_config_t *config) {
  // process general settings
  if (process_general_settings(conf, config) < 0) {
    // return error
    return -1;
  }
  // process keybinds
  if (process_keybindings(conf, config) < 0) {
    // return error
    return -1;
  }
  // return success
  return 0;
}

/*
 * load_kbdm_config
 *
 * load config from config file into a kbdm_config_t
 *
 * path arg is an absolute path to config
 * config arg is a pointer to a kbdm_config_t
 *
 * return 0 on success
 * return -1 on failure
 */
static int
load_kbdm_config(const char *path,
                 kbdm_config_t *config) {
  // init return
  int res = 0;
  // init libconfig
  config_t conf;
  config_init(&conf);
  // load config from file
  res = config_read_file(&conf, path);
  // check for errors
  if (res == CONFIG_FALSE) {
    // return error
    return -1;
  }
  // process config
  res = process_kbdm_config(&conf, config);
  // teardown libconfig
  config_destroy(&conf);
  // return result
  return res;
}

int load_config(const char *path,
                kbdm_config_t *config) {
  // load config
  if (load_kbdm_config(path, config) < 0) {
    // return error
    return -1;
  }
  // return success
  return 0;
}
