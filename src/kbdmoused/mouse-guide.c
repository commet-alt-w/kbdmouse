#include "mouse-guide.h"
#include "mouse.h"
#include "cursor-move.h"
#include "scheduler.h"
#include "event.h"
#include "../kbdm.h"
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <libevdev/libevdev-uinput.h>

/*
 * get_next_event
 *
 * get next kbdm event from the event queue
 */
static kbdm_event_t *
get_next_event(void) {
  // init return
  kbdm_event_t *e = NULL;
  // wait for event
  e = pull_event();
  // return
  return e;
}

/*
 * get_key_state
 *
 * use libevdev event to determine key state
 *
 * return pressed state if value is 1 or 2 (pressed/held)
 * return released state otherwise
 */
static int
get_key_state(int value) {
  if (value == 1 || value == 2) {
    return PRESSED;
  } else {
    return RELEASED;
  }
}

/*
 * move_cursor
 *
 * move cursor by provided delta
 */
static void
move_cursor(const kbdm_event_t *event,
            const cursor_info_t *cvs) {
  // get value of event
  int key_state = get_key_state(event->ev->value);
  // check the event code type
  switch(event->ev->code) {
  case KEY_UP:
    *cvs[UP].key_state = key_state;
    break;
  case KEY_RIGHT:
    *cvs[RIGHT].key_state = key_state;
    break;
  case KEY_DOWN:
    *cvs[DOWN].key_state = key_state;
    break;
  case KEY_LEFT:
    *cvs[LEFT].key_state = key_state;
    break;
  }
}

/*
 * process_events
 *
 * process events from the event queue
 */
static void
process_events(const cursor_info_t *cvs) {
  // loop until thread is canceled
  while (true) {
    // get event
    kbdm_event_t *event = get_next_event();
    // move mouse
    move_cursor(event, cvs);
    // free memory
    free(event->ev);
    free(event);
    // null event
    event = NULL;
  }
}

/*
 * start_kbdm_mouse_guide
 *
 * begin processing events
 */
static int
start_kbdm_mouse_guide(const cursor_info_t *cvs) {
  // process events
  process_events(cvs);
  
  return 0;
}

void *
kbdm_mouse_guide_handle(void *args) {
  // get cursor vars
  const cursor_info_t *cvs = args;
  
  // init thread state
  init_thread_state();
  
  // get events
  if (start_kbdm_mouse_guide(cvs) < 0) {
    return NULL;
  }
  
  return args;
}
