#include <stdbool.h>
#include <syslog.h>
#include <errno.h>
#include <string.h>

void kbdmlogerr(const char * const str, bool strerr) {
  syslog(LOG_ERR, str);
  if (strerr) {
    syslog(LOG_ERR, "ERROR: %s", strerror(errno));
  }
}

void kbdmlog(const int type, const char * const str) {
  syslog(type, str);
}
