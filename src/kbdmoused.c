#include "kbdmoused.h"
#include "kbdm.h"
#include "kbdmoused/scheduler.h"
#include "kbdmoused/daemon.h"
#include "kbdmoused/config.h"
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>
#include <syslog.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

static const int signals[] = {SIGINT, SIGTERM};
static bool no_daemon = false;

/*
 * print_log_err_exit
 *
 * print msg to stderr, log msg to error syslog,
 * and exit program
 *
 * msg arg is a message to be sent to stderr and syslog
 */
static void
print_log_err_exit(const char *msg, bool strerr) {
  // print error to stderr
  fprintf(stderr, "%s\n", msg);
  if (strerr) {
    fprintf(stderr, "ERROR: %s\n", strerror(errno));
  }
  // log error
  kbdmlogerr(msg, strerr);
  // cleanup config
  cleanup_config();
  // exit failure
  exit(EXIT_FAILURE);
}

/*
 * root_check
 *
 * if process was not started with uid/gid 0
 * print/log error and exit failure
 */
static void
root_check(void) {
  uid_t ruid;
  gid_t rgid;

  // get real uid/gid
  ruid = getuid();
  rgid = getgid();

  // check if running with elevated privileges
  if (ruid != 0 && rgid != 0) {
    // no root, do not start, print/log error
    const char * const errmsg = "kbdmouse requires root privileges";
    print_log_err_exit(errmsg, false);
  }
}

/*
 * init_daemon
 *
 * daemonize process, lock pid file
 */
static void
init_daemon(void) {
  // daemonize
  if (daemonize() == -1) {
    const char * const errmsg = "cannot daemonize kbdmoused";
    print_log_err_exit(errmsg, true);
  }

  // prevent other daemons from starting with pid file lock
  if(lock_daemon(KBDMD_PIDFILE,
                 KBDMD_PIDFLAGS,
                 KBDMD_PIDMODE) == -1) {
    const char * const errmsg = "cannot pid file lock";
    print_log_err_exit(errmsg, true);
  }
}

/*
 * start_scheduler
 *
 * start scheduler
 */
static void
init_scheduler(void) {
  // scheduler 
  if (start_kbdm_scheduler() < 0) {
    const char * const errmsg = "cannot start scheduler";
    print_log_err_exit(errmsg, true);
  }
}

/*
 * shutdown_handle
 *
 * handle to be called when program recieves a signal
 * which is meant to invoke a shutdown of the process
 */
static void
shutdown_handle(UNUSED int signal) {
  // log
  switch(signal) {
  case SIGINT:
    DKBDMLOGINFO("got sigint, shutting down");
    break;
  case SIGTERM:
    DKBDMLOGINFO("got sigterm, shutting down");
    break;
  }
  // scheduler cleanup
  scheduler_cleanup();
  // cleanup config
  cleanup_config();
  // exit program
  exit(EXIT_SUCCESS);
}

/*
 * init_signal_handling
 *
 * set signal handling for main process
 */
static void
init_signal_handling(void) {
  int cshr = 0;
  // set signals for shuttding down process
  for (uint i = 0; i < sizeof(signals)/sizeof(uint); i++) {
    // get change signal result
    cshr = change_signal_handle(signals[i], shutdown_handle);
    // check for errors
    if (cshr < 0) {
      const char * const errmsg = "cannot change SIGTERM handle";
      print_log_err_exit(errmsg, true);
    }
  }
}

/*
 * config_check
 *
 * check for config file. if config cannot be found print/log
 * error and exit failure
 */
static void
config_check(void) {
  // make sure config file is present on system
  if (!config_present()) {
    // print/log error and exit
    const char * const errmsg = "config file not found";
    print_log_err_exit(errmsg, true);
  }
}

/*
 * init_kbdm_config
 *
 * initailize config. print error and exit failure if
 * there were any errors
 */
static void
init_kbdm_config(void) {
  // initialize config
  if (init_config() < 0) {
    // print/log error and exit
    const char * const errmsg = "cannot init config";
    print_log_err_exit(errmsg, true);
  }
}

/*
 * start_kbdmoused
 *
 * daemonize, open log, main loop
 */
static void
start_kbdmoused(void) {
  // open log
  openlog("kbdmoused", LOG_ODELAY, LOG_DAEMON);

  // check config
  config_check();

  // init config
  init_kbdm_config();

  // check uid/gid for elevated privileges
  root_check();

  // init daemon
  if (!no_daemon) {
    init_daemon();
  }

  // init sigterm handle
  init_signal_handling();

  // start scheduler
  init_scheduler();

  // main loop
  while(true) {
#ifdef DEBUG
    kbdmlog(LOG_INFO, "kbdmoused running");
#endif
    sleep(10);
  }
}

/*
 * print_usage
 *
 * print usage string to stdout
 */
static void
print_usage(void) {
  printf("%s\n", USAGE_STR);
}

/*
 * print_help
 *
 * print help string to stdout
 */
static void
print_help(void) {
  printf("%s\n", HELP_STR);
}

/*
 * set_config_path
 *
 * initialize config path. exit program with failure
 * if unable. print/log error
 */
static void
set_config_path(const char *path) {
  // init config file
  if (init_config_file(path) < 0) {
    // print/log error and exit
    const char * const errmsg = "cannot load config";
    print_log_err_exit(errmsg, true);
  }
}

/*
 * parse_opts
 *
 * parse cli options
 */
static void
get_opts(int argc, char **argv) {
  int opt;
  // loop looking for command line options
  while ((opt = getopt(argc, argv, CLI_OPTS)) != -1) {
    switch(opt) {
    case OPT_CONFIG_PATH:
      // config path provided
      set_config_path(optarg);
      break;
    case OPT_NO_DAEMON:
      // no daemon
      no_daemon = true;
      break;
    case OPT_HELP:
      // print help/usage and exit
    default:
      print_usage();
      print_help();
      exit(EXIT_SUCCESS);
    }
  }
}

int main (int argc, char **argv) {
  // parse cli options
  get_opts(argc, argv);
  // start program
  start_kbdmoused();
  // exit
  exit(EXIT_FAILURE);
}
