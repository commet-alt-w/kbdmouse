#ifndef KBDM_H
#define KBDM_H

#include <stdbool.h>
#include <syslog.h>

// unused attribute macro
#define UNUSED __attribute__((unused))

// debug log info macro
#ifdef DEBUG
#define DKBDMLOGINFO(str) kbdmlog(LOG_INFO, str)
#else
#define DKBDMLOGINFO(str)
#endif

/*
 * kbdmlogerr
 *
 * syslog LOG_ERR with string
 * print ERROR with errno to string
 */
void kbdmlogerr(const char * const str, bool strerr);

/*
 * kbdmlog
 *
 * decouple logging implementation from application code
 */
void kbdmlog(const int type, const char * const str);

#endif
