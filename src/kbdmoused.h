#ifndef KBDMOUSED_H
#define KBDMOUSED_H

#define KBDMD_PIDFILE "/var/run/kbdmoused.pid"
#define KBDMD_PIDFLAGS (O_RDWR|O_CREAT)
#define KBDMD_PIDMODE (S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH|S_ISGID)
#define CLI_OPTS "c:xh"
#define OPT_CONFIG_PATH 'c'
#define OPT_NO_DAEMON 'x'
#define OPT_HELP 'h'
#define USAGE_STR "usage:\n  kbdmoused [options...]"
#define HELP_STR ("help options:\n"\
                  "  -c config path\n"\
                  "  -x no daemon\n"\
                  "  -h this help")

int main(int argc, char **argv);

#endif
