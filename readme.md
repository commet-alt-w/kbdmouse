# kbdmouse

keyboard mouse
  - control mouse with keyboard arrows

## requirements

- `eudev-dev`
- `libevdev-dev`

## building

- release
  - `$ make` or `$ make release`
- debug
  - `$ make debug`

## running

- release
  - `$ ./build/release/kbdmoused`
- debug
  - `$ ./build/debug/kbdmoused`

## debugging

- gdb
  - daemonized
    1. `$ gdb ./build/debug/kbdmoused`
    1. `(gdb) set follow-fork-mode child`
  - no daemon
    1. `$ gdb ./build/debug/kbdmoused`
    1. `(gdb) r -x`

## unit tests

- setup
  - using [criterion](https://github.com/Snaipe/Criterion)
  - follow [setup instructions](https://criterion.readthedocs.io/en/master/setup.html)
  - add criterion's `include` directory to `$PATH`
  - add criterion's `build/src` directory to `$LD_LIBRARY_PATH`
  - optional
    - set environment variable
      - `CRITERION_VERBOSITY_LEVEL=1`
- building
  - `$ make test`
- running
  - `$ ./build/test/kbdmouse-unit-tests`
  - `$ ./build/test/kbdmouse-unit-tests --verbose`

## checking for memory leaks

- using valgrind
  - daemonized
    1. `$ ./check-for-memory-leaks -c 1 -b ./build/debug/kbdmoused`
    1. `$ pkill valgrind` or `$ kill -15 $(pgrep valgrind)`
  - no daemon
    1. `$ ./check-for-memory-leaks -c 1 -b ./build/debug/kbdmoused -p "-x"`
    1. `<ctrl>+c`

## license

[gnu gpl v3](/license)
