#toolchain.cmake
# cmake toolchain file

# c compiler and standard
set(CMAKE_C_COMPILER "/usr/bin/gcc")
set(CMAKE_C_STANDARD "11")
set(CMAKE_C_STANDARD_REQUIRED YES)
set(CMAKE_EXPORT_COMPILE_COMMANDS 1)

# cflags
set(CMAKE_C_FLAGS "-Wall -Wextra -Wunreachable-code -Wpedantic"
  CACHE STRING "cmake cflags")

# cflags for debug build
set(CMAKE_C_FLAGS_DEBUG "-O0 -DDEBUG -g3 -ggdb3"
  CACHE STRING "cmake debug cflags")

# cflags for unit tests build
set(CMAKE_C_FLAGS_TEST "${CMAKE_C_FLAGS_DEBUG}"
  CACHE STRING "cmake test cflags")

# cflags for release build
set(CMAKE_C_FLAGS_RELEASE "-O2 -DNDEBUG"
  CACHE STRING "cmake relase cflags")

# cflags for release with debug info
set(CMAKE_C_FLAGS_RELWITHDEBINFO "-O2 -g -ggdb -DDEBUG"
  CACHE STRING "cmake relwithdebinfo cflags")

# linker flags for release build
set(CMAKE_EXE_LINKER_FLAGS_RELEASE "-Wl,--strip-debug"
  CACHE STRING "cmake release linker flags")

# linker flags for test build
set(CMAKE_EXE_LINKER_FLAGS_TEST
  "-Wl,--unresolved-symbols=ignore-all -Wl,-zmuldefs -no-pie"
  CACHE STRING "cmake test linker cflags")
